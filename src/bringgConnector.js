const CryptoJS = require(  'crypto-js' );
const rp = require(  'request-promise' );

/*
 * request-promise with a default 'POST' method.
 */
const rpPost = ({uri, body}) => rp({
  method: 'POST',
  uri,
});

module.exports= ({
  bringgURL = 'http://developer-api.bringg.com/partner_api',
  accessToken = '',
  secret = '',
}) => {

  /*
   * recieve query params and append a timestamp, access token and a signature with the secret.
   */
  const signedQueryParams = ({
    params,
  }) => {
    const queryParams = Object.keys(params).map( param =>
      `${param}=${encodeURIComponent(params[param])}`
    ).concat('access_token=' + encodeURIComponent(accessToken))
      .concat('timestamp=' + Date.now())
      .join('&');
    return queryParams.concat('&signature=' + CryptoJS.HmacSHA1(queryParams, secret));
  };

  /*
   * function for creating a url for the bringg api.
   * recieves a resource and the query params which it will sign.
   */

  const bringgURLCreator = ({
    resource,
    params = {},
  }) => (`${bringgURL}/${resource}?${signedQueryParams({params})}`);

  /*
   * recieves customer parameters. If a customer with the same phone
   * number already exists, retrieve him. If not, create one.
   */
  const createIfNotExists = ({
    name,
    address,
    phone,
  }) => (
    rp(bringgURLCreator({
      resource: `customers/phone/${phone}`,
    })).catch( err => {
      if (err.statusCode === 404) {
        return rpPost({
          uri: bringgURLCreator({
            resource: 'customers',
            params: {
              name,
              phone,
              address,
            },
          }),
        })
      }
    } ).then(JSON.parse)
  );

  /*
   * created a new Order for a customer.
   * if the customer does not exist, it will create him
   */
  const placeOrder = ({
    title = '',
    address,
    phone,
    name,
  }) => (
    createIfNotExists({
      address,
      phone,
      name,
    }).then( data => 
      rpPost({
        uri: bringgURLCreator({
          resource: 'tasks',
          params: {
            title,
            address,
            customer_id: data.customer.id,
          },
        }),
      }).then(JSON.parse))
  );

  /*
   * get the orders of the customer with the given phone that were created in the last 7 days
   */
  const getOrders = phone => {
    return overcomePagination('tasks').then(arr => arr.filter(by('phone', phone)));
  };

  /*
   * A filter function. returns true if the order is in the last 7 days and the customer's "prop" is "value"
   */
  const by = ( prop, value ) => order => thisWeek(order.created_at) && order.customer[prop] === value;

  /*
   * returns true if the date is in the last 7 days
   */
  const thisWeek = old => {
    const timeNow = (new Date()).getTime();
    const oldTime = (new Date(old)).getTime();
    const timeInDay = 1000 * 60 * 60 * 24;
    return Math.ceil(( timeNow - oldTime)/(timeInDay)) <= 7
  }

  /*
   * Recursive function that will bring all pages of a bringg resource
   */

  const overcomePagination = (resource, page = 1, all = []) => {
    return rp(bringgURLCreator({
      resource,
      params: {
        page,
      },
    })).then(JSON.parse).then( data => {
      if( data.length === 50 ) {
        return overcomePagination(resource, page + 1, all.concat(data))
      }
      return all.concat(data);
    } )
  }

  return {placeOrder, getOrders};

}
