const Express = require('express');
const app = Express();
const bringg = require('./bringgConnector.js');
const { placeOrder, getOrders } = bringg({
  accessToken: process.env.ACCESS,
  secret: process.env.SECRET,
});
const port = process.env.PORT || 8080;

app.post('/orders', (req, res) => {
  const name = req.query.name;
  const phone = req.query.phone;
  const address = req.query.address;
  if( !name ){
    return res.status(400).send('Please provide us with your name');
  }
  if( !phone ){
    return res.status(400).send('We must be able to contact you. Please provide a phone number');
  }
  if( !address ){
    return res.status(400).send('Where do you want us to send the message? Please provide an address')
  }
  placeOrder({
    title: req.query.title,
    name,
    phone,
    address,
  }).then( order => res.send(`Your order was recieved. the order id is ${order.task.id}`) )
    .catch( err => {
      return res.status(err.statusCode).send('An error has occured');
    } );
});

app.get('/orders', (req, res) => {
  const phone = req.query.phone;
  if( !phone ){
    return res.status(400).send('Your phone number is like your id. We must have it to identify you');
  }
  getOrders(phone).then(res.json.bind(res));
})

app.listen( port, () => console.log(`listening on ${port}`) )
