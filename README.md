## Starting the server

run it with 
```
ACCESS=token SECRET=secret npm start
```
One may add the PORT env. variable if wishes.


## Trying it out

```
curl -X POST "http://localhost:8080/orders?title=great&phone=%2B15206809353&name=beautiful&address=amazing"
```

Will add a new order under the customer with the given phone number, with the title, name and address given.
**All but the title are required.**

If the customer does not exist, one with the given parameters will be created
```
curl "http://localhost:8080/orders?phone=%2B152"
```
Will fetch all the orders in the last 7 days that were placed by the customer with the given phone number.
**Phone number is required.**

If the customer does not exist, an empty array is returned  without errors.